This package will install [Laravel Socialite](https://laravel.com/docs/master/socialite) and add routes to your project to handle loging in with a Social network platform. You can easily configure what networks are available through a config file. This package includes simple views for use in your project, along with an sass file with social platform color brands for your project.

This package has checks to insure that only the configured networks are accessible. Users can upgrade their account to a social platform for easier access. Users can not register multiple networks with the same email address to prevent account spoofing.

### Package Installation:
Require package
```php
composer require strangefate/socialrouter
```

Run migration to add platform variables to the user database
```php
php artisan migrate
```

Modify the User model so the new fields are fillable
```php
//old
protected $fillable = [
    'name', 'email', 'password',
];

//new - option 1
protected $fillable = [
    'name', 'email', 'password','platform','platform_id','platform_image'
];

//new - option 2
protected $guarded = ['email_verified_at', 'remember_token'];
```

You can test if the package is properly installed by checking the route list in your project. You should see two new routes for **/auth/{provider}** and an **/auth/logout** route

```php
php artisan route:list
```

### Configuration
Several default platforms are available in the social router package, please see the [Laravel Socialite](https://laravel.com/docs/master/socialite) documentation and the Social Network's platform for details on what supported and how to configure them. Publish the config file to configure the platforms you want your application to support. The config file also contains snippets for adding variables to your services config and env file.
```php
php artisan vendor:publish --provider=StrangeFate\SocialRouter\SocialRouterServiceProvider --tag=config
```
Remove the providers you don't want from the **socialrouter.php** file configure the access tokens in your app's **config\services.php** . Only providers you list in the config file will be supported, anything not in the config file will be dealt a 404 error.

### Assets
You can publish the sass and view files to customize the templates for your site.
```php
php artisan vendor:publish --provider=StrangeFate\SocialRouter\SocialRouterServiceProvider --tag=assets
```

The sass file contains a standard collection of social platforms brand colors for your project. Add this document to your sass project and it will create classes for anchor tags in the platforms color or buttons with that platform as a background.
```cmd
resources\sass\socialcolors.scss
```

The **socialcolors.scss** is a sass file that has brand colors for all your projects. Add this to your sass compilation to have them added to your style sheet.

```scss
@import "socialcolors";
```

```html
<a class="twitter" href="#">Twitter</a>
<button class="button facebook">Facebook</button>
```

### View templates
The social router comes with pre-made templates for you to include in your project. Templates are based off of Zurb Foundation Sites Libraries. Customize these to fit your platform.
```blade
@include('socialrouter::menu-list-default')
@include('socialrouter::inline-list')
@include('socialrouter::menu-list-vertical')
@include('socialrouter::button-list')
```

![Example view template display](https://bitbucket.org/strangefate/socialrouter/raw/82d57a29e8118dd4aafcd12bd48c68e957f90cd1/docs/socialmenus.PNG)
