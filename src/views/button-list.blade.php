<h4>Log in with your social account</h4>
@forelse( config('socialrouter.networks') as  $name => $provider)
	<a href="/auth/{{ $name }}/redirect" class="button {{$name}} expanded">
		Log in with <i class="{{ $provider["class"] }}"></i> {{ $provider["name"] }}
	</a>
@empty
	There are no profiders configured
@endforelse