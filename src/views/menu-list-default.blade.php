<ul class="menu align-center icons icon-left">
	<li class="menu-text">
		Join with:
	</li>
	@forelse( config('socialrouter.networks') as  $name => $provider)
		<li>
			<a href="{{ url("/auth/$name/redirect") }}" class="{{ $name }}">
				<i class="{{ $provider["class"] }} "></i> {{ $provider["name"] }}
			</a>
		</li>
	@empty
		There are no profiders configured
	@endforelse
</ul>