Join with:
@forelse( config('socialrouter.networks') as  $name => $provider)
	<a href="{{ url("/auth/$name/redirect") }}" class="{{ $name }}">
		<i class="{{ $provider["class"] }} "></i>
	</a>
@empty
	There are no profiders configured
@endforelse