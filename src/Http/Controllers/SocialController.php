<?php

namespace StrangeFate\SocialRouter\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function redirect($driver) {
		//checks if $drive is in valid social network
		$driver = strtolower($driver);
		if( !array_key_exists( $driver, config('socialrouter.networks') ) )
			abort(404, "The application does not support this type of login");
		
		//redirect to platform auth page
		return Socialite::driver($driver)->redirect();
	}

	public function signin($driver) {
		//checks if $drive is in valid social network
		$driver = strtolower($driver);
		if( !array_key_exists($driver, config('socialrouter.networks') ) )
			abort(404, "The application does not support this type of login");

		//Update or create the user
		$account = Socialite::driver($driver)->user();

		if( $this->checkIfBinding($driver) )
			$this->bindUserAccount($account);

		elseif ( ! $this->checkIfAccountExistsWithDifferentMethod($account, $driver) ) {
			$user = User::updateOrCreate(
				[	
					'email' => $account->email, 
					'platform' => $driver
				],
				[
					'platform_id' => $account->id, 
					'name' => $account->name, 
					'platform_image' => $account->avatar, 
					'password' => 'social login'
				]
			);

			//log the user in.
			auth()->login($user, true);
		}

		return redirect('/');
	}

	public function bind($driver) {
		session()->put('socialBind', $driver);

		return redirect("/auth/$driver/redirect");
	}

	public function destroy() {
		auth()->logout();
		return redirect('/');
	}

	private function checkIfBinding($driver) {
		return auth()->check() 
			&& session()->has('socialBind')
			&& session()->get('socialBind') == $driver;
	}

	private function bindUserAccount($account) {
		$user = auth()->user();
		$driver = session()->pull('socialBind');

		if( $user->email == $account->email) {
			$user->platform = $driver;
			$user->platform_id = $account->id;
			$user->name = $account->name;
			$user->platform_image = $account->avatar;
			$user->password = 'social login';
			$user->save();
		} else
			abort(403, "Email of social account does not match account of current user logged in.");
	}

	private function checkIfAccountExistsWithDifferentMethod($account, $driver) {
		$users = User::where('email', 'like', $account->email)->where('platform', 'not like', $driver)->count();

		if( $users > 0 )
			abort(403, "There is already an account bound with this email address.");
	}
}