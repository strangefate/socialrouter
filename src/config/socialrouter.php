<?php 
return [
	'networks' => [
		"google" => [
			"name" => "Google",
			"class" => "fab fa-google",
		],
		"facebook" => [
			"name" => "Facebook",
			"class" => "fab fa-facebook",
		],
		"twitter" => [
			"name" => "Twitter",
			"class" => "fab fa-twitter",
		],
		"linkedin" => [
			"name" => "LinkedIn",
			"class" => "fab fa-linkedin",
		],
		"github" => [
			"name" => "GitHub",
			"class" => "fab fa-github",
		],
		"gitlab" => [
			"name" => "GitLab",
			"class" => "fab fa-gitlab",
		],
		"bitbucket" => [
			"name" => "BitBucket",
			"class" => "fab fa-bitbucket",
		],
	]
];

/* Copy these items into config\services.php for Laravel Socialite configuration. ENV entriest are below.

	'google' => [
		'client_id' => env('GOOGLE_OATH_CLIENT_ID'),
		'client_secret' => env('GOOGLE_OATH_CLIENT_SECRET'),
		'redirect' =>  env('APP_URL') . '/auth/google'
	],

	'facebook' => [
		'client_id' => env('FACEBOOK_APP_ID'),
		'client_secret' => env('FACEBOOK_APP_SECRET'),
		'redirect' =>env('APP_URL') . '/auth/facebook'
	],

	'twitter' => [
		'client_id' => env('TWITTER_CLIENT_ID'),
		'client_secret' => env('TWITTER_CLIENT_SECRET'),
		'redirect' => env('APP_URL') . '/auth/twitter'
	],

	"linkedin" => [
		'client_id' => env('LINKEDIN_CLIENT_ID'),
		'client_secret' => env('LINKEDIN_CLIENT_SECRET'),
		'redirect' => env('APP_URL') . '/auth/linkedin',
	],

	"github" => [
		'client_id' => env('GITHUB_CLIENT_ID'),
		'client_secret' => env('GITHUB_CLIENT_SECRET'),
		'redirect' => env('APP_URL') . '/auth/github',
	],

	"gitlab" => [
		'client_id' => env('GITLAB_CLIENT_ID'),
		'client_secret' => env('GITLAB_CLIENT_SECRET'),
		'redirect' => env('APP_URL') . '/auth/gitlab',
	],

	"bitbucket" => [
		'client_id' => env('BITBUCKET_CLIENT_ID'),
		'client_secret' => env('BITBUCKET_CLIENT_SECRET'),
		'redirect' => env('APP_URL') . '/auth/bitbucket',
	],
*/


/*ENV FILE ENTRIES

Find the keys for the social login packages you wish to use in 
your application and copy them to your env file and supply the values.

GOOGLE_OATH_CLIENT_ID=
GOOGLE_OATH_CLIENT_SECRET=
FACEBOOK_APP_ID=
FACEBOOK_APP_SECRET=
TWITTER_CLIENT_ID=
TWITTER_CLIENT_SECRET=
LINKEDIN_CLIENT_ID=
LINKEDIN_CLIENT_SECRET=
GITHUB_CLIENT_ID=
GITHUB_CLIENT_SECRET=
GITLAB_CLIENT_ID=
GITLAB_CLIENT_SECRET=
BITBUCKET_CLIENT_ID=
BITBUCKET_CLIENT_SECRET=

*/