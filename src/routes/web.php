<?php

Route::group(['namespace' => 'StrangeFate\SocialRouter\Http\Controllers', 'middleware' => ['web'] ], function() {
	Route::get('/auth/logout', 'SocialController@destroy')->middleware('auth');
	Route::get('/auth/{provider}', 'SocialController@signin');
	Route::get('/auth/{provider}/redirect', 'SocialController@redirect');
	Route::get('/auth/{provider}/bind', 'SocialController@bind')->middleware('auth');
});