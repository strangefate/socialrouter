<?php 
namespace StrangeFate\SocialRouter;

use Illuminate\Support\ServiceProvider;

class SocialRouterServiceProvider extends ServiceProvider
{
	public function boot() {
		$this->loadRoutesFrom( __DIR__.'/routes/web.php' );
		$this->mergeConfigFrom(__DIR__.'/config/socialrouter.php', 'socialrouter' );
		$this->loadMigrationsFrom(__DIR__.'/migrations');
		$this->loadViewsFrom(__DIR__.'/views', 'socialrouter');

		$this->publishes([
			__DIR__.'/config/socialrouter.php' => config_path('socialrouter.php'),
		], 'config');

		$this->publishes([
			__DIR__.'/resources/sass/socialcolors.scss' => resource_path('sass/socialcolors.scss'),
			__DIR__.'/views' => resource_path('views/vendor/socialrouter')
		], 'assets');
	}

	public function register() {

	}
}